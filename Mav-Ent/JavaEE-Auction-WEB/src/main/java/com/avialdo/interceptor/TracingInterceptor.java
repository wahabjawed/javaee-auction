/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.interceptor;

import java.io.Serializable;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author abdulwahab
 */
@Interceptor
public class TracingInterceptor implements Serializable{
  
    @AroundInvoke
    public Object logCall(InvocationContext context) throws Exception{
        System.out.println("Invoking method: " + context.getMethod());
        return context.proceed();
    }
}