/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdulwahab
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Delivery.findAll", query = "SELECT d FROM Delivery d"),
    @NamedQuery(name = "Delivery.findById", query = "SELECT d FROM Delivery d WHERE d.id = :id"),
    @NamedQuery(name = "Delivery.findByCreatedAt", query = "SELECT d FROM Delivery d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "Delivery.findByCreatedAt1", query = "SELECT d FROM Delivery d WHERE d.status = 0 and d.createdAt < :createdAt"),
    @NamedQuery(name = "Delivery.findByCreatedAt2", query = "SELECT d FROM Delivery d WHERE d.status = 1 and d.createdAt < :createdAt"),
    @NamedQuery(name = "Delivery.findByOrderid", query = "SELECT d FROM Delivery d WHERE d.orderid = :orderid"),
    @NamedQuery(name = "Delivery.findByProductName", query = "SELECT d FROM Delivery d WHERE d.productName = :productName"),
    @NamedQuery(name = "Delivery.findByUserId", query = "SELECT d FROM Delivery d WHERE d.userId = :userId"),
    @NamedQuery(name = "Delivery.findByStatus", query = "SELECT d FROM Delivery d WHERE d.status = :status")})
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int orderid;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int productName;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int status;

    public Delivery() {
    }

    public Delivery(Integer id) {
        this.id = id;
    }

    public Delivery(Integer id, int orderid, int productName, int userId, int status) {
        this.id = id;
        this.orderid = orderid;
        this.productName = productName;
        this.userId = userId;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public int getProductName() {
        return productName;
    }

    public void setProductName(int productName) {
        this.productName = productName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Delivery)) {
            return false;
        }
        Delivery other = (Delivery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.avialdo.entity.Delivery[ id=" + id + " ]";
    }
    
}
