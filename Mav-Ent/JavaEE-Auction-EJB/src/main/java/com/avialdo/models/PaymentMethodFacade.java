/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.PaymentMethod;
import com.avialdo.entity.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class PaymentMethodFacade extends AbstractFacade<PaymentMethod> implements PaymentMethodFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaymentMethodFacade() {
        super(PaymentMethod.class);
    }

    @Override
    public PaymentMethod getCard(int userId) {

        try {
            PaymentMethod method = getEntityManager().createNamedQuery("PaymentMethod.findByUserId", PaymentMethod.class).setParameter("userId", userId).getSingleResult();

            return method;
        } catch (NoResultException e) {
            return null;
        }

    }
}
