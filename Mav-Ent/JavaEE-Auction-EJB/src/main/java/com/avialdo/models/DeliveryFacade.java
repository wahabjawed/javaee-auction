/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Delivery;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class DeliveryFacade extends AbstractFacade<Delivery> implements DeliveryFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DeliveryFacade() {
        super(Delivery.class);
    }

    @Override
    public List<Delivery> findAllUserId(Integer id) {
         try {
            List<Delivery> a;
            a = getEntityManager().createNamedQuery("Delivery.findByUserId", Delivery.class).setParameter("userId", id).getResultList();
            return a;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }
    
}
