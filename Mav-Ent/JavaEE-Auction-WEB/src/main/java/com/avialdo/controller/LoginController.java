/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.entity.User;
import com.avialdo.models.UserFacadeLocal;
import java.util.Date;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author abdulwahab
 */
@Named("loginController")
@ManagedBean
@RequestScoped
public class LoginController {

    String email;
    String password;

    @EJB
    private UserFacadeLocal userFacade;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String save() {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = userFacade.getLoginUser(email, password);
        if (user != null) {
            Date curr = new Date();
            if (user.getWithdrawnPenaltyDate() != null && curr.before(user.getWithdrawnPenaltyDate())) {
                context.addMessage(null,
                        new FacesMessage("Account Blocked"));
                return "login";
            } else {
                context.getExternalContext().getSessionMap().put("user", user);
                return "/dashboard.xhtml?faces-redirect=true";
            }
        } else {
            context.addMessage(null,
                    new FacesMessage("Invalid Credentials"));
            return "login";
        }

    }
}
