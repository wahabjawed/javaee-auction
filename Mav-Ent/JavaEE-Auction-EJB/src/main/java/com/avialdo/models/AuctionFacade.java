/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Auction;
import com.avialdo.entity.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class AuctionFacade extends AbstractFacade<Auction> implements AuctionFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuctionFacade() {
        super(Auction.class);
    }

    @Override
    public List<Auction> findByUserId(Integer userId) {
        try {
            List<Auction> a;
            a = getEntityManager().createNamedQuery("Auction.findByUserId", Auction.class).setParameter("userId", userId).getResultList();
            return a;
        } catch (NoResultException e) {
            return null;
        }

    }

    @Override
    public List<Auction> findAllByProduct(Integer productId) {
        try {
            List<Auction> a;
            a = getEntityManager().createNamedQuery("Auction.findByProductId", Auction.class).setParameter("productId", productId).getResultList();
            return a;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void concludeProduct(List<Product> productIds) {
        for(Product productId : productIds){
            try {
                List<Auction> list;
                list = getEntityManager().createNamedQuery("Auction.findByProductId", Auction.class).setParameter("productId", productId.getId()).getResultList();
                if (list.size() == 1) {
                    Auction a = list.get(0);
                    a.setStatus(1);
                    AuctionFacade.super.edit(a);
                } else {
                    Auction max = list.get(0);
                    max.setStatus(1);
                    AuctionFacade.super.edit(max);
                    list.remove(max);
                    for (Auction l : list) {
                        l.setStatus(1);
                        AuctionFacade.super.edit(l);
                    }
                }
            }catch (NoResultException e) {
                
            }
        }
    }
    
   @Override
   public Double getHighestBid(int prodId){
        try {
               TypedQuery<Double> query = getEntityManager().createNamedQuery("Auction.findByHighestBid", Double.class).setParameter("productId", prodId);
               Double a = query.getSingleResult();
               return a;
           } catch (NoResultException e) {
               return null;
           }
    }

    @Override
    public void callFlush() {
        getEntityManager().flush();
    }
   
   
 
}
