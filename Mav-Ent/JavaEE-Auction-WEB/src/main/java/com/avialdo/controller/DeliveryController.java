/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.entity.Delivery;
import com.avialdo.entity.Product;
import com.avialdo.entity.User;
import com.avialdo.models.DeliveryFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author abdulwahab
 */
@Named("deliveryController")
@SessionScoped
public class DeliveryController implements Serializable {

    @EJB
    private DeliveryFacadeLocal deliveryFacade;

    List<Product> productList = new ArrayList<>();

    public DeliveryController() {

    }

    public List<Delivery> findAll() {

        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        return deliveryFacade.findAllUserId(user.getId());

    }

}
