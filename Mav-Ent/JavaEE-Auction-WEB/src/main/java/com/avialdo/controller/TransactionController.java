/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.entity.Transaction;
import com.avialdo.entity.User;
import com.avialdo.models.TransactionFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author abdulwahab
 */
@Named("transactionController")
@SessionScoped
public class TransactionController implements Serializable {

     @EJB
    private TransactionFacadeLocal transactionFacade;

    public TransactionController() {

    }

    public List<Transaction> findAll() {

        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        return transactionFacade.findAllUserId(user.getId());

    }
}
