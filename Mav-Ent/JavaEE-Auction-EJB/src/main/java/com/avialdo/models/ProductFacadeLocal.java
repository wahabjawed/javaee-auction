/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Product;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author abdulwahab
 */
@Local
public interface ProductFacadeLocal {

    void create(Product product);

    void edit(Product product);

    void remove(Product product);

    Product find(Object id);

    List<Product> findAll();
    List<Product> findAllUserId(Integer userId);

    List<Product> findRange(int[] range);
    List<Product> findByArrayId (List<Integer> productArrayId );
    List<Product> findAllExpireDate(Date date);
    
    int count();

    public void generateRandom(Integer x);
    
    List<Product> findNameLike(String name);
    
    List<Product> findProdCategory(String selectedCat);
    
    List<Product> findProdSubCategory(String selectedSubCat);
    
    public Product getDescription(int id);
    
}
