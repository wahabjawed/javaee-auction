/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.constant.AppConfig;
import com.avialdo.entity.Product;
import com.avialdo.entity.User;
import com.avialdo.interceptor.TracingInterceptor;
import com.avialdo.models.ProductFacadeLocal;
import com.avialdo.models.UserFacadeLocal;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author abdulwahab
 */
@Named("productController")
@SessionScoped
@Interceptors(TracingInterceptor.class)
public class ProductController implements Serializable {

    @EJB
    private ProductFacadeLocal productFacade;
    
    @EJB
    private UserFacadeLocal userFacade;

    
    
    private Product product = new Product();

    List<Product> productList = new ArrayList<>();

    List<String> category;

    List<String> subCategory = new ArrayList<>();

    String searchInput;

    Integer currentProductId;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductController() {

    }

    public List<Product> findAll() {
        

        Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (requestParameterMap != null && requestParameterMap.size() > 0 && requestParameterMap.containsKey("type") && requestParameterMap.get("type").equals("self")) {
            User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
            return productFacade.findAllUserId(user.getId());
        } else {
            return productFacade.findAll();
        }
    }
    
    
    @Timeout
    public List<Product> generateRandomProduct() {
        List<Product> c = new ArrayList<>();
        try {
            List<Product> b;
            b = productFacade.findAll();
            if(b.size()>3){
            
            Random random = new Random();
            int rand = random.nextInt(b.size() - 1);
            List<Integer> numbersSelected = new ArrayList<>();
             int  numberOfProm = 3;

            for (int i = 0; i < numberOfProm; i++) {
                c.add(b.get(rand));
                numbersSelected.add(rand);
                while (numbersSelected.contains(rand)) {
                    rand = random.nextInt(b.size() - 1);
                }

            }
           }else {
            return b;
            }
            
        } catch (NoResultException e) {
            return null;
        }
        return c;
    }

    public String save() {

        product.setCreatedBy(AppConfig.getInstance().getUserID());
        productFacade.create(product);
        //return "/dashboard.xhtml?faces-redirect=true";
        currentProductId = product.getId();
        return "/productDescription.xhtml?faces-redirect=true";
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public List<String> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<String> subCategory) {
        this.subCategory = subCategory;
    }

    @PostConstruct
    public void init() {
        category = AppConfig.getInstance().getCategory();
    }

    public void populateSubCategory(AjaxBehaviorEvent event) {
        if (product.getCategory() != null) {
            subCategory.clear();
            subCategory.addAll(AppConfig.getInstance().getSubCategory().get(product.getCategory()));
        }
    }

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            String relativeWebPath = "/upload";
            String absoluteDiskPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(relativeWebPath);
            UploadedFile uploadedFile = event.getFile();
            String fileName = FilenameUtils.getBaseName(uploadedFile.getFileName());
            String extension = FilenameUtils.getExtension(uploadedFile.getFileName());
            File file = File.createTempFile(fileName + "_", "." + extension, new File(absoluteDiskPath));

            product.setImageURL(fileName);

            InputStream input = uploadedFile.getInputstream();

            //Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Uploaded file successfully saved in " + file);
            FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");

            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String searchItem() {
        productList = productFacade.findNameLike(searchInput);
        getCurrentProdList();
        return "searchresults.xhtml?faces-redirect=true";
    }

    public String searchItemByCategory() {
        String selectedCat = product.getCategory();
        String selectedSubCat = product.getSubCategory();

        if (selectedSubCat != null) {
            productList = productFacade.findProdSubCategory(selectedSubCat);
        } else {
            productList = productFacade.findProdCategory(selectedCat);
        }
        getCurrentProdList();
        return "searchresults.xhtml?faces-redirect=true";
    }

    public String getSearchInput() {
        return searchInput;
    }

    public void setSearchInput(String searchInput) {
        this.searchInput = searchInput;
    }

//    public List<Product> getListProducts() {
//        return listProducts;
//    }
//
//    public void setListProducts(List<Product> listProducts) {
//        this.listProducts = listProducts;
//    }
    public String getProduct(Integer prodId,Product prod) {
        product = prod;
        //Product p = productFacade.find(prodId);
        currentProductId = prodId;
        return "productDescription.xhtml?faces-redirect=true";
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public Integer getCurrentProductId() {
        return currentProductId;
    }

    public void setCurrentProductId(Integer currentProductId) {
        this.currentProductId = currentProductId;
    }

    private void getCurrentProdList() {

        List<Product> prodList = new ArrayList<>();

        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");

        Date dateNow = new Date();
        for (int i = 0; i < productList.size(); ++i) {
            Date dateBid = null;
            if (productList.get(i).getDueDate() != null) {
                try {
                    dateNow = sdfo.parse(sdfo.format(dateNow));
                    dateBid = sdfo.parse(sdfo.format(productList.get(i).getDueDate()));
                } catch (ParseException ex) {
                    Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (dateBid.after(dateNow) || sdfo.format(dateNow).equals(sdfo.format(productList.get(i).getDueDate()))) {
                    prodList.add(productList.get(i));
                }
            }
        }
        productList = prodList;
    }

    public String getProdDueDate(Date prodDueDate) {
        SimpleDateFormat sdfo = new SimpleDateFormat("dd-MM-yyyy");
        return sdfo.format(prodDueDate);
    }
    
    public String getProductDetail(int id){
        product = this.productFacade.getDescription(id);
        return product.getName();
    }
    
    public String getCreatedBy(){
        
        User u = userFacade.find(product.getCreatedBy());
        String fName = u.getFirstName();
        String lName = u.getLastName();
        return fName+' '+lName;
    }
    
    public int getCurrentUserId(){
        return AppConfig.getInstance().getUserID();
    }
    
    public String withdrawProduct(Integer prodId){
        Product prod = productFacade.find(prodId);
        productFacade.remove(prod);
        return "/removedItem.xhtml?faces-redirect=true";
    }
}
