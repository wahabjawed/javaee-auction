/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.constant.AppConfig;
import com.avialdo.entity.PaymentMethod;
import com.avialdo.entity.User;
import com.avialdo.models.PaymentMethodFacadeLocal;
import com.avialdo.models.UserFacadeLocal;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author abdulwahab
 */
@Named("profileController")
@ManagedBean
@RequestScoped
public class ProfileController {

    @EJB
    UserFacadeLocal userFacede;

    private User user = new User();

    @EJB
    PaymentMethodFacadeLocal paymentFacede;

    private PaymentMethod payment = new PaymentMethod();
    
    boolean createNew = false;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PaymentMethod getPayment() {
        return payment;
    }

    public void setPayment(PaymentMethod payment) {
        this.payment = payment;
    }

    @PostConstruct
    public void init() {
        user = userFacede.find(AppConfig.getInstance().getUserID());

        payment = paymentFacede.getCard(AppConfig.getInstance().getUserID());

        if (payment == null) {
            payment = new PaymentMethod();
            createNew = true;
        }
    }

    public String saveProfile() {

        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().replace("user", user);
        userFacede.edit(user);
        return "/updateProfile.xhtml?faces-redirect=true";
    }

    public String savePayment() {

        if (createNew) {
            payment.setUserId(AppConfig.getInstance().getUserID());
            paymentFacede.create(payment);
        } else {
            paymentFacede.edit(payment);
        }
        return "/updateProfile.xhtml?faces-redirect=true";
    }

}
