/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.entity.User;
import com.avialdo.models.UserFacadeLocal;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author abdulwahab
 */
@Named(value = "registerController")
@ManagedBean
@RequestScoped
public class RegisterController {

    private String retypePassword;

    private User user = new User();

    @EJB
    private UserFacadeLocal userFacade;

    public String getRetypePassword() {
        return retypePassword;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String save() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (!user.getPassword().equals(retypePassword)) {
            context.addMessage(null,
                    new FacesMessage("Password MixMatch"));
            return "register";
        } else if (!userFacade.isEmailUsed(user.getEmail())) {
            context.addMessage(null,
                    new FacesMessage("Email is associated with another account"));
            return "register";
        } else {
            userFacade.create(user);
            context.getExternalContext().getSessionMap().put("user", user);
            return "/login.xhtml?faces-redirect=true";

        }

    }
}
