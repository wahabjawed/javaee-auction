/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class TransactionFacade extends AbstractFacade<Transaction> implements TransactionFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransactionFacade() {
        super(Transaction.class);
    }

    @Override
    public List<Transaction> findAllUserId(Integer id) {
        try {
            List<Transaction> a;
            a = getEntityManager().createNamedQuery("Transaction.findByUserId", Transaction.class).setParameter("userId", id).getResultList();
            return a;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }
}
