/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Auction;
import com.avialdo.entity.Product;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> implements ProductFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    @Override
    public List<Product> findByArrayId(List<Integer> productArrayId) {
        try {
            List<Product> a;
            TypedQuery<Product> query = getEntityManager().createNamedQuery("Product.findByArrayId", Product.class).setParameter("arrayId", productArrayId);

            a = query.getResultList();
            return a;
        } catch (NoResultException e) {
            return null;
        }

    }

    @Override
    public void generateRandom(Integer x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Product> findAllUserId(Integer userId) {
        try {
            List<Product> list;
            list = getEntityManager().createNamedQuery("Product.findByCreatedBy", Product.class).setParameter("createdBy", userId).getResultList();
            return list;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }
    
    
    @Override
    public List<Product> findAllExpireDate(Date date) {
        try {
            List<Product> list;
            list = getEntityManager().createNamedQuery("Product.findByDueDate", Product.class).setParameter("dueDate", date).getResultList();
            return list;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }
    
    @Override
    public List<Product> findNameLike(String name){
        try {
               TypedQuery<Product> query = getEntityManager().createNamedQuery("Product.findByNameLike", Product.class).setParameter("name", "%"+name+"%");
               List<Product> list = query.getResultList();
               return list;
           } catch (NoResultException e) {
               return null;
           }
    }   

    @Override
    public List<Product> findProdCategory(String selectedCat) {
        try {
               TypedQuery<Product> query = getEntityManager().createNamedQuery("Product.findByCategory", Product.class).setParameter("category", selectedCat);
               List<Product> list = query.getResultList();
               return list;
           } catch (NoResultException e) {
               return null;
           }
    }

    @Override
    public List<Product> findProdSubCategory(String selectedSubCat) {
        try {
               TypedQuery<Product> query = getEntityManager().createNamedQuery("Product.findBySubCategory", Product.class).setParameter("subCategory", selectedSubCat);
               List<Product> list = query.getResultList();
               return list;
        } catch (NoResultException e) {
               return null;
        }
    }
    
    @Override
    public Product getDescription(int id){
        try {
            return getEntityManager().createNamedQuery("Product.findById", Product.class).setParameter("id", id).getSingleResult();
        } catch (Exception e) {
        }
        return null;  
    } 

}
