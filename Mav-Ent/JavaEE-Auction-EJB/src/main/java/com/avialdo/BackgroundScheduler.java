/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo;

import com.avialdo.entity.Auction;
import com.avialdo.entity.Delivery;
import com.avialdo.entity.Product;
import com.avialdo.models.AuctionFacade;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulwahab
 */
@Singleton
public class BackgroundScheduler {

//    @EJB
//    private AuctionFacade auctionFacade;
    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Schedule(hour = "0", minute = "0", second = "0", persistent = false)
    public void updatePromotion() {
        // Do your job here which should run every start of day.
    }

    @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
    public void concludeProduct() {

        System.out.println("DeclarativeScheduler:: In concludeProduct()");
        try {
            List<Product> list;
            list = em.createNamedQuery("Product.findByDueDate", Product.class).setParameter("dueDate", new Date()).getResultList();

            for (Product productId : list) {

                List<Auction> lists;
                lists = em.createNamedQuery("Auction.findByProductId", Auction.class).setParameter("productId", productId.getId()).getResultList();
                if (lists != null && lists.size() > 0) {
                    if (lists.size() == 1) {
                        Auction a = lists.get(0);
                        a.setStatus(1);
                        em.merge(a);
                    } else {
                        Auction max = lists.get(0);
                        max.setStatus(1);
                        em.merge(max);
                        lists.remove(max);
                        for (Auction l : lists) {
                            l.setStatus(2);
                            em.merge(l);
                        }
                    }
                }
                productId.setIsActive(0);
                em.merge(productId);

            }
        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }
    }

    @Schedule(second = "*/15", minute = "*", hour = "*", persistent = false)
    public void DeliveryUpdateOne() throws InterruptedException {
        System.out.println("DeclarativeScheduler:: In DeliveryUpdateOne()");
        try {
            List<Delivery> a;
            a = em.createNamedQuery("Delivery.findByCreatedAt1", Delivery.class).setParameter("createdAt", new Date()).getResultList();

                for (Delivery d : a) {
                    d.setStatus(1);
                    em.merge(d);
                }
        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }
    }

    @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
    public void DeliveryUpdateTwo() throws InterruptedException {
        System.out.println("DeclarativeScheduler:: In DeliveryUpdateTwo()");
        try {
            List<Delivery> a;
            a = em.createNamedQuery("Delivery.findByCreatedAt2", Delivery.class).setParameter("createdAt", new Date()).getResultList();

                for (Delivery d : a) {
                    d.setStatus(2);
                    em.merge(d);
                }
        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }
    }

}
