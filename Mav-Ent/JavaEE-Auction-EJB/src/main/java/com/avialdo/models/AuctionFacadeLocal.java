/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.Auction;
import com.avialdo.entity.Product;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author abdulwahab
 */
@Local
public interface AuctionFacadeLocal {

    void create(Auction auction);

    void edit(Auction auction);

    void remove(Auction auction);

    Auction find(Object id);

    List<Auction> findAll();

    List<Auction> findRange(int[] range);

    int count();
    
    List<Auction> findByUserId (Integer userId);
    
    List<Auction> findAllByProduct(Integer productId);
    
    void concludeProduct(List<Product> productIds);
    
    Double getHighestBid(int prodId);
    
    void callFlush();
    
}
