/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.models;

import com.avialdo.entity.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulwahab
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {

    @PersistenceContext(unitName = "JavaEE-Auction-Unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User getLoginUser(String email, String password) {
        try {
            User user = getEntityManager().createNamedQuery("User.hasAnAccount", User.class).setParameter("email", email).setParameter("password", password).getSingleResult();
            return user;
        } catch (NoResultException e) {
            return null;
        }
    }
    

    @Override
    public boolean isEmailUsed(String email) {
        return getEntityManager().createNamedQuery("User.findByEmail", User.class).setParameter("email", email).getResultList().isEmpty();

    }
}
