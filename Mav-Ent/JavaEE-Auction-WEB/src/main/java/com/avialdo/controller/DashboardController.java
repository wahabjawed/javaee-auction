/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avialdo.controller;

import com.avialdo.entity.User;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author abdulwahab
 */
@Named("dashboardController")
@SessionScoped
public class DashboardController implements Serializable {

    public DashboardController() {
        user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    }

    User user = new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login.xhtml?faces-redirect=true";
    }
}
