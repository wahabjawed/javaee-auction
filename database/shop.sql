-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 17, 2020 at 10:31 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `auction`
--

CREATE TABLE `auction` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT current_timestamp(),
  `status` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AUCTIONSTATUSTYPE`
--

CREATE TABLE `AUCTIONSTATUSTYPE` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT current_timestamp(),
  `orderid` int(11) NOT NULL,
  `productName` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `status` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `buyerId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `auctionId` int(11) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT current_timestamp(),
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orderStatusLog`
--

CREATE TABLE `orderStatusLog` (
  `id` int(11) NOT NULL,
  `orderId` int(11) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT current_timestamp(),
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderStatusType`
--

CREATE TABLE `orderStatusType` (
  `id` int(4) NOT NULL,
  `type` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paymentMethod`
--

CREATE TABLE `paymentMethod` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `num` varchar(20) DEFAULT NULL,
  `expireDate` varchar(5) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `ccv` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paymentMethod`
--

INSERT INTO `paymentMethod` (`id`, `name`, `num`, `expireDate`, `userId`, `ccv`) VALUES
(1, 'Abdul Wahab', '1234-1233-1231-2312', '12/34', 1, 123);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `createdBy` int(11) DEFAULT NULL,
  `isActive` int(1) DEFAULT 1,
  `dueDate` date DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `subCategory` varchar(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `askPrice` double DEFAULT 0,
  `imageURL` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `createdOn`, `createdBy`, `isActive`, `dueDate`, `category`, `subCategory`, `description`, `askPrice`, `imageURL`) VALUES
(1, 'test', '2019-11-20 09:45:00', 1, NULL, NULL, 'Mobile', 'smasung', 'hgh', 56, NULL),
(2, 'wahab', '2019-11-20 09:45:00', 1, NULL, '2019-11-21', 'Mobile', 'nokia', 'test ', 123, NULL),
(3, 'test', '2019-11-20 09:49:18', 1, NULL, '2019-11-28', 'Mobile', 'nokia', 'desc', 123, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(3) NOT NULL,
  `userId` int(3) NOT NULL,
  `description` varchar(100) NOT NULL,
  `amount` varchar(6) NOT NULL,
  `status` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(4) NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 1,
  `address` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `withdrawnPenalty` int(11) DEFAULT 0,
  `withdrawnPenaltyDate` timestamp NULL DEFAULT NULL,
  `profileImage` varchar(200) DEFAULT NULL,
  `rowType` int(5) DEFAULT 0,
  `isActive` int(1) DEFAULT 1,
  `createdOn` timestamp NULL DEFAULT current_timestamp(),
  `isVerified` int(1) DEFAULT 0,
  `verifyCode` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `email`, `password`, `type`, `address`, `phone`, `withdrawnPenalty`, `withdrawnPenaltyDate`, `profileImage`, `rowType`, `isActive`, `createdOn`, `isVerified`, `verifyCode`) VALUES
(1, 'Abdul', 'Wahab', 'wahabjawed@gmail.com', 'test', 0, '1 liffy road, Liffey Valley Park', '0876008895', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction`
--
ALTER TABLE `auction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `AUCTIONSTATUSTYPE`
--
ALTER TABLE `AUCTIONSTATUSTYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderStatusLog`
--
ALTER TABLE `orderStatusLog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderStatusType`
--
ALTER TABLE `orderStatusType`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentMethod`
--
ALTER TABLE `paymentMethod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auction`
--
ALTER TABLE `auction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `AUCTIONSTATUSTYPE`
--
ALTER TABLE `AUCTIONSTATUSTYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderStatusLog`
--
ALTER TABLE `orderStatusLog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderStatusType`
--
ALTER TABLE `orderStatusType`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentMethod`
--
ALTER TABLE `paymentMethod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
